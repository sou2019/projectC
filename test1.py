print("hello world")
x=5 
y="john" 
#print(x+y)

b="abcde"
c=b[1:3]
print(c)


# val=input("Enter your value: ") 
# print(val)

thislist = ["a","b","c"]
print(thislist[1])


thislist = ["apple", "banana", "cherry"]
if "apple" in thislist:
  print("Yes, 'apple' is in the fruits list")


for x in thislist:
  print(x)


a = 33
b = 33
if b > a:
  print("b is greater than a")
elif a==b:
  print("equal")   


f=open("input.txt", "w+")
for i in range(10):
  f.write("This is line %d\r\n" % (i+1))
f.close()

dict = {"name":"stan", "id":"123"}
print(dict);

def my_function():
  print("Hello from a function")

my_function();
